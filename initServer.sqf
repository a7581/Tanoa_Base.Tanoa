//	setDate [2021, 8, 10, 4, 30];
	
	///////////////////////////////////////////////////
	//Nimmt Russen die total bescheuerten Brillen weg//
	///////////////////////////////////////////////////
	
	{if ((side _x) != west) then {removeGoggles _x};}foreach allUnits;

	// /////////////////////////////////////////
	// //Fzg Loadout ca. 1/4 Truppstartloadout//
	// /////////////////////////////////////////
	
	//San
	{
		_x addItemCargoGlobal ["BWA3_30Rnd_556x45_G36_AP",6];
		_x addItemCargoGlobal ["BWA3_15Rnd_9x19_P8",4];
		_x addItemCargoGlobal ["ACE_fieldDressing",20];
		_x addItemCargoGlobal ["ACE_elasticBandage",20];
		_x addItemCargoGlobal ["ACE_packingBandage",20];
		_x addItemCargoGlobal ["ACE_quikclot",20];
		_x addItemCargoGlobal ["ACE_tourniquet",10];
		_x addItemCargoGlobal ["ACE_morphine",10];
		_x addItemCargoGlobal ["ACE_epinephrine",10];
		_x addItemCargoGlobal ["ACE_CableTie",4];
		_x addItemCargoGlobal ["Chemlight_green",2];
		_x addItemCargoGlobal ["ACE_surgicalKit",4];
		_x addItemCargoGlobal ["ACE_salineIV_250",4];
		_x addItemCargoGlobal ["ACE_salineIV_500",4];
		_x addItemCargoGlobal ["ACE_salineIV",4];
		_x addItemCargoGlobal ["ACE_bodyBag",10];
		_x addItemCargoGlobal ["adv_aceCPR_AED",1];
	} 
	forEach [san_veh_1, san_veh_2];
	
	//Wiesel
	{
		_x addItemCargoGlobal ["BWA3_OpsCore_Fleck", 2];
	} 
	forEach [wiesel_mk20_1, wiesel_mk20_2, wiesel_mk20_3, wiesel_mk20_4, wiesel_mk20_5, wiesel_mk20_6];

	//Ammotrucks
	{
		_x setVariable ["ACE_isRepairVehicle", true, true];
	}
	forEach [truck_1];
	
	//////////////////
	//Spritverbrauch//
	//////////////////

	[[

		san_veh_1,
		san_veh_2,
		truck_1,
		wiesel_mk20_1,
		wiesel_mk20_2,
		wiesel_mk20_3,
		wiesel_mk20_4,
		wiesel_mk20_5,
		wiesel_mk20_6

	],7.5] call Fett_Redd_fnc_FuelConsumption;

	[[
		
		heli_1,
		heli_2,
		plane_1
	],5] call Fett_Redd_fnc_FuelConsumption;

	////////////////////////////////////////
	//Munikisten ca. 1 Gruppenstartloadout//
	////////////////////////////////////////

	//AT
	{
		
		_x addItemCargoGlobal ["BWA3_PzF3_Tandem_Loaded", 3];
	}
	forEach [at_muni_1,at_muni_2,at_muni_3,at_muni_4,at_muni_5,at_muni_6];

	//Inf
	{
	
		_x addItemCargoGlobal ["BWA3_30Rnd_556x45_G36_Tracer", 7];
		_x addItemCargoGlobal ["BWA3_30Rnd_556x45_G36_AP", 28];
		_x addItemCargoGlobal ["BWA3_200Rnd_556x45", 10];
		_x addItemCargoGlobal ["BWA3_15Rnd_9x19_P8", 20];
		_x addItemCargoGlobal ["BWA3_DM51A1", 8];
		_x addItemCargoGlobal ["BWA3_DM25", 17];
		_x addItemCargoGlobal ["hlc_20rnd_762x51_b_G3", 5];
		_x addItemCargoGlobal ["ACE_CableTie",8];

	}
	forEach 
	[
		
		inf_muni_1,
		inf_muni_2,
		inf_muni_3,
		inf_muni_4,
		inf_muni_5,
		inf_muni_6

	];
	
	{
	
		_x addItemCargoGlobal ["BWA3_DM51A1", 32];
		_x addItemCargoGlobal ["BWA3_DM25", 64];
		_x addItemCargoGlobal ["BWA3_DM32_Purple", 8];
		_x addItemCargoGlobal ["BWA3_DM32_Red", 8];
		_x addItemCargoGlobal ["BWA3_DM32_Green", 8];
		_x addItemCargoGlobal ["1Rnd_HE_Grenade_shell",36];
		_x addItemCargoGlobal ["1Rnd_SmokeRed_Grenade_shell",15];
		_x addItemCargoGlobal ["1Rnd_SmokeGreen_Grenade_shell",15];
		_x addItemCargoGlobal ["1Rnd_SmokePurple_Grenade_shell",15];

	}
	forEach 
	[
		
		gr_muni_1,
		gr_muni_2
	];

	//AusrüstungsKisten
	{

		_x addItemCargoGlobal ["ACE_CableTie", 20];
		_x addItemCargoGlobal ["ACE_Flashlight_MX991", 10];
		_x addItemCargoGlobal ["Rangefinder", 10];
		_x addItemCargoGlobal ["ACE_DK10_b", 5];
		_x addItemCargoGlobal ["ACE_GD300_b", 5];
		_x addItemCargoGlobal ["TFAR_anprc152", 10];
		_x addItemCargoGlobal ["ACE_EntrenchingTool", 10];
		_x addItemCargoGlobal ["ToolKit", 3];
		_x addItemCargoGlobal ["ACE_MapTools", 5];
		_x addItemCargoGlobal ["ACE_HelmetCam", 5];
		_x addItemCargoGlobal ["Laserbatteries", 2];
		_x addItemCargoGlobal ["Laserdesignator_03", 2];
		_x addItemCargoGlobal ["ACE_RangeCard", 3];
		_x addItemCargoGlobal ["ACE_microDAGR", 2];
		
	}
	forEach [equi_1];
	{
		
		_x addBackpackCargoGlobal ["BWA3_Carryall_Fleck", 3];
		_x addBackpackCargoGlobal ["TFAR_rt1523g_big_bwmod", 3];
		_x addBackpackCargoGlobal ["BWA3_AssaultPack_Fleck", 3];
		
	}
	forEach [equi_2];

	//SaniKisten
	{

		_x addItemCargoGlobal ["ACE_fieldDressing", 90];
		_x addItemCargoGlobal ["ACE_elasticBandage", 90];
		_x addItemCargoGlobal ["ACE_packingBandage", 90];
		_x addItemCargoGlobal ["ACE_quikclot", 90];
		_x addItemCargoGlobal ["ACE_tourniquet", 30];
		_x addItemCargoGlobal ["ACE_morphine", 30];
		_x addItemCargoGlobal ["ACE_epinephrine", 30];
		_x addItemCargoGlobal ["ACE_surgicalKit", 12];
		_x addItemCargoGlobal ["ACE_salineIV_250", 12];
		_x addItemCargoGlobal ["ACE_salineIV_500", 12];
		_x addItemCargoGlobal ["ACE_salineIV", 12];
		_x addItemCargoGlobal ["ACE_bodyBag",10];
		
	}
	forEach [sanKiste_1,sanKiste_2,sanKiste_3,sanKiste_4,sanKiste_5,sanKiste_6];
	
	[] spawn
	{
		sleep 30;
		
		[inf_muni_box,100] call ace_cargo_fnc_setSpace; 
		[inf_muni_box,[inf_muni_1,inf_muni_2,inf_muni_3,inf_muni_4,inf_muni_5,inf_muni_6]] spawn compile preprocessFileLineNumbers "scripts\loadIn.sqf";

		[at_muni_box,100] call ace_cargo_fnc_setSpace; 
		[at_muni_box,[at_muni_1,at_muni_2,at_muni_3,at_muni_4,at_muni_5,at_muni_6]] spawn compile preprocessFileLineNumbers "scripts\loadIn.sqf";
		
		[supply_box,100] call ace_cargo_fnc_setSpace; 
		[supply_box,[gr_muni_1,gr_muni_2,equi_1,equi_2]] spawn compile preprocessFileLineNumbers "scripts\loadIn.sqf";
		
		[sani_box,100] call ace_cargo_fnc_setSpace; 
		[sani_box,[sanKiste_1,sanKiste_2,sanKiste_3,sanKiste_4,sanKiste_5,sanKiste_6]] spawn compile preprocessFileLineNumbers "scripts\loadIn.sqf";
	};